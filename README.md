# redsky_retail #

## Overview

This project is the coding homework submission for Target's SEM role.
It attempts to strike a balance between clarity, simplicity, and production-readiness.
In general, where there are tradeoffs to be made, I'm erring on the side of simplicity and clarity.
This is most apparent in dealing with asynchronous APIs - I'm generally calling the async API and then blocking immediately on the future. This would not be a good pattern to follow for code running at scale.

## Components ##
* Java 8
* Scala 2.6
* Scalatra REST framework 2.6 (which brings in several dependencies)
* sttp REST client framework (wraps several scala HTTP clients, including akka)
* MongoDB 4

## Prerequisites ##
This codebase assumes it's being built and run on macOS 10.12+.
It assumes that Java8 and sbt are installed and in the user's path.
It assumes that mongodb 4.x is installed but not running.

## Build & Run ##

A shell script is provided to start a fresh instance of mongodb then compile and fire up the app.

```sh
$ setup.bash
```

Then go to [http://localhost:8080/](http://localhost:8080/) in your browser.

## Design approach ##

REST calls are routed through the class `RedSkyRetail` class by scalatra. This class defines `get` and `put` REST handlers for fetching product info or updating price info. There are three handlers of interest:
* `GET /` returns a basic health check indicating whether the db connection exists.
* `GET /test` returns a json response based on marshalling a hard-coded domain object to ensure the transformation from domain object to json works as expected.
* `GET /products/:id` checks whether the argument is convertable to a long. If not, returns a 400 BadRequest response. If so, attempts to fetch product info from the underlying REST service. If product info is found, returns it. If not, returns a 404 NotFound.
* `PUT /products/:id` checks whether the argument is convertable to a long.  If not, returns a 400 BadRequest response. If so, attempts to parse the request body as a json price message. If this is successful, attempts to persist the price to mongodb. If that's successful, returns a 204 NoContent response. If not, returns a 400 BadRequest.
The `RedSkyRetail` class also holds a connection to mongodb, which is passed into business logic code when that code needs access to persistence.

Underneath the REST handler are several other pieces of code. 
* `RedSkyHTTPClient` encapsulates the HTTP client call out to the redsky.target.com service. It also holds JSON parsing logic. It exposes methods that are intended to be easily callable from the REST layer.
* `ProductPricePersistence` encapsulates the logic needed to store and retrieve price data from the NoSQL data store. All of the public methods here take in the db connection as a `Option[MongoDatabase]` for testability; if they're called with a `None` value for the option, rather than a live connection, they'll return a `None` Option back rather than failing. This allows unit tests for other parts of the app to be run without needing an active mongodb instance.
* Lastly, the model package contains three classes for representing product and price info, with some utility methods for creating instances of these classes from various types of input.

There are also a variety of tests provided. I didn't know the mongodb API when I started this project, so I took a somewhat test-driven-development approach to learning it. Some of the tests aren't relevant to the actual function of this project and could now be removed, but they were useful in providing some confidence that the low-level persistence subsystem was behaving as expected.

## Changes needed for production use ##
There are some shortcuts in this code that would make it not quite ready for prime time:
* Logging should be configured. Current logging is just default to stdout.
* Mongo's api is all async, but the calls to it are all treated as synchronous. There could be performance gains to be had by leveraging the asynch
* The underlying http client is also using synchronous/blocking calls. The http client framework allows use of the akka asynch framework; there could be performance gains to be had.
* The app provides a very rudimentary health check, a more sophisticated health check should be provided including data such as # of requests serviced, avg time to handle, # of failures, and anything else the devops team thinks is useful to track.
* The JSON parsing in the `parseRedSkyProductInfo` and `parsePriceInfo` methods is concise but might be brittle and might be slow. If it turns out to be slow, then there are likely faster ways to extract the pieces of the json response needed by the app. However, the simplicity of the parsing code also has value.
* Not an architectural change, but this code has not yet undergone peer review, which should be a gating function for anything going to production.


