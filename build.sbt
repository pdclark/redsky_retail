val ScalatraVersion = "2.6.3"

organization := "net.pclark"

name := "redsky_retail"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.6"

resolvers += Classpaths.typesafeReleases

javaOptions ++= Seq(
  "-Xdebug",
  "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"
)

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.4.9.v20180320" % "container",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.json4s"   %% "json4s-jackson" % "3.5.2",
  "com.softwaremill.sttp" %% "core" % "1.3.0",
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.0"
)

enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)

// allow jetty to continue running after sbt exits
containerShutdownOnExit := false
