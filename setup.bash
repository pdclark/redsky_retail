#! /bin/bash

MONGO_ROOT=/usr/local/mongodb-osx-x86_64-4.0.1
MONGO_DATA=mongo_data

if [ -s $MONGO_DATA/mongod.lock ]; then
	echo "Shutting down mongod"
	killall mongod
	sleep 2
fi

echo "Cleaning up old db files"
rm -rf $MONGO_DATA
mkdir $MONGO_DATA

echo "Starting mongodb server"
$MONGO_ROOT/bin/mongod --bind_ip localhost --dbpath $MONGO_DATA &

echo "Waiting for mongo to start"
# a better move would be to watch the mongodb log files for the "[initandlisten] waiting for connections on port 27017" message
sleep 5

# insert some predefined test data into mongo so that the scala tests have something to look for
echo "inserting mongo test data"
$MONGO_ROOT/bin/mongo redsky --eval "db.test.insert({num:1, info:\"I am test data\", items:[1,2,3] } )"
$MONGO_ROOT/bin/mongo redsky --eval "db.prices.insert({pid:-1, price: NumberDecimal(12.75), currency:'USD'} )"
# create a unique index so that we never have more than one pid
$MONGO_ROOT/bin/mongo redsky --eval "db.prices.createIndex({\"pid\":-1}, {unique:true})"

# start the service
echo "starting web server"
sbt jetty:quickstart

# To test raw price insertion - can't do a PUT through the browser, but can do this instead from the command-line:
# curl -X PUT -H "Content-Type: application/json" -d '{"id":5678,"current_price":{"value": 66.42,"currency_code":"EUR"}}' http://localhost:8080/products/5678