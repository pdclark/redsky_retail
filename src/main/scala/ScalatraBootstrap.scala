import net.pclark._
import org.scalatra._

import org.mongodb.scala._

import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {
  // make mongo available to controllers.
  // normally, this would be abstracted into a trait and the connection info either read from a config file or the environment.
  val mongoClient: MongoClient = MongoClient("mongodb://localhost")

  override def init(context: ServletContext) {
    val database: MongoDatabase = mongoClient.getDatabase("redsky")
    val db = Some(database)

    context.mount(new RedSkyRetail(db), "/*")
  }

  override def destroy(context:ServletContext) {
    mongoClient.close()
  }
}
