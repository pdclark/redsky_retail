package net.pclark

import net.pclark.model._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.bson.{BsonReader, BsonWriter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.bson.codecs.configuration.CodecRegistries._
import org.bson.types.Decimal128
import org.mongodb.scala.model.UpdateOptions
import org.mongodb.scala.result.UpdateResult

object ProductPricePersistence {

  // sadly, the scala mongodb driver doesn't support converting between mongo's decimal128 and scala's BigDecimal types out of the box.
  // This inner class defines the encoder for that. It's embedded here since it's only relevant for talking to mongo.
  class BigDecimalScalaCodec extends Codec[scala.math.BigDecimal] {

    override def encode(writer: BsonWriter, value: scala.math.BigDecimal, encoderContext: EncoderContext): Unit = {
      writer.writeDecimal128(new Decimal128(value.bigDecimal))
    }

    override def getEncoderClass: Class[scala.math.BigDecimal] = classOf[scala.math.BigDecimal]

    override def decode(reader: BsonReader, decoderContext: DecoderContext): scala.math.BigDecimal = {
      reader.readDecimal128().bigDecimalValue()
    }
  }

  private val codecRegistry = fromRegistries(fromCodecs(new BigDecimalScalaCodec()), fromProviders(classOf[PersistentPrice]), DEFAULT_CODEC_REGISTRY )

  def getCollection(db:MongoDatabase):MongoCollection[PersistentPrice] = {
    val dbWithCodecs = db.withCodecRegistry(codecRegistry)
    val c:MongoCollection[PersistentPrice] = dbWithCodecs.getCollection("prices")

    c
  }

  private def _fetchPriceForId(db:MongoDatabase, id:Long) : PersistentPrice = {
    val collection = getCollection(db)
    val findQuery = collection.find(equal("pid", id))
    val result = Await.result(findQuery.head(), 5 seconds)
    result
  }

  // return an Option[BigDecimal] - if the result is None, it means that either there's no db connection or the item wasn't found.
  def fetchPriceForID(db:Option[MongoDatabase], id:Long) : Option[BigDecimal]  = {
    db match {
      case None => None
      case Some(d) => {
        val priceResult = _fetchPriceForId(d, id)
        if (priceResult == null) {
          None
        } else {
          Some(priceResult.price)
        }
      }
    }

  }

  // will insert the price if new, or update if we've already seen this pid
  // shouldn't happen, given the upsert behavior, but there's also a unique index on pid
  // so an attempt to insert a second record for the same pid will fail.
  private def _savePriceForId(db:MongoDatabase, price:PersistentPrice) : result.UpdateResult = {
    val collection = getCollection(db)
    val updateQuery = collection.updateOne(equal("pid", price.pid),
      combine(set("price", price.price), set("currency", price.currency), setOnInsert("pid", price.pid)),
      new UpdateOptions().upsert(true))
    val result = Await.result(updateQuery.head(), 5 seconds)

    result
  }

  /* Price will default to USD */
  def savePriceForId(db:Option[MongoDatabase], id:Long, price:BigDecimal, currency:String = "USD") : Option[result.UpdateResult] = {
    db match {
      case None => None
      case Some(d) => {
        val newPrice = PersistentPrice(id, price, currency)
        Some(_savePriceForId(d, newPrice))
      }
    }
  }

  def savePriceInfoforId(db:Option[MongoDatabase], id:Long, priceinfo:PriceInfo) : Option[result.UpdateResult] = {
    savePriceForId(db, id, priceinfo.value, priceinfo.currency_code)
  }


    def clearPrices(db:Option[MongoDatabase], id:Long) : Option[result.DeleteResult] = {
      db match {
        case None => None
        case Some(d) => {
          val collection = getCollection(d)
          val deleteQuery = collection.deleteMany(equal("pid", id))
          val result = Await.result(deleteQuery.head(), 5 seconds)
          Some(result)
        }
      }
    }
}
