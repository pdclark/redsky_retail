package net.pclark

import com.softwaremill.sttp._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import net.pclark.model.{PriceInfo, ProductInfo}
import org.mongodb.scala.MongoDatabase

trait RedSkyHTTPClient {
  val baseUrl = "http://redsky.target.com/v2/pdp/tcin"
  val defaultExcludes = Some("taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics")

  // pull in json4s formatters
  protected implicit lazy val localJsonFormats:Formats = DefaultFormats.withBigDecimal

  def getRawProductInfo(productId: Long) = {

    val request = sttp.get(uri"$baseUrl/$productId?excludes=$defaultExcludes")

    // the HttpURLConnectionBackend is synchronous/blocking; this would be inappropriate for a high-volume production client.
    implicit val backend = HttpURLConnectionBackend()
    val response = request.send()

    // response.body is an Either - the left side is populated if there's an error, the right side is populated if not.
    if (response.isSuccess)
      (response.code, response.body.right.get)
    else
     (response.code, "")
  }

  def fetchProductInfo(db:Option[MongoDatabase], productId: Long) = {
    val (response, data) = getRawProductInfo(productId)

    // do we have pricing info for this product?
    val result = response match {
      case 404 => ProductInfo.notFound(productId)
      case 200 => {
        val pi = parseRedSkyProductInfo(data)
        val price = ProductPricePersistence.fetchPriceForID(db, productId)
        pi.setPrice(price)
      }
      case _ => ProductInfo.notFound(-1)
    }
    (response, result)
  }

  // this method should be considered private, and only exposed for testing
  def parseRedSkyProductInfo(raw:String) : ProductInfo = {

    // this JSON parsing is not particularly robust; it will fail if the JSON format returned by redsky changes
    val ast = parse(raw)
    val name = (ast \ "product" \ "item" \ "product_description" \ "title").extract[Option[String]]
    val price = (ast \ "product" \ "price" \ "listPrice" \ "price").extract[Option[BigDecimal]]
    val id = (ast \ "product" \ "item" \ "tcin").extract[Option[String]]

    ProductInfo.parse(id, name, price)
  }

  def parsePriceInfo(raw:String) : PriceInfo = {
    // this JSON parsing is not particularly robust; this might be better handled by a json parser integrated with the PriceInfo case class
    val ast = parse(raw)
    val currency = (ast \ "current_price" \ "currency_code").extract[Option[String]]
    val price = (ast \ "current_price" \ "value").extract[Option[BigDecimal]]

    PriceInfo.parse(price, currency)
  }


}
