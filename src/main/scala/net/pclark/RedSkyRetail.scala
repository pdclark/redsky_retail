package net.pclark

import net.pclark.model.ProductInfo
import org.scalatra._
import org.json4s.{DefaultFormats, Formats}
import org.mongodb.scala.MongoDatabase
import org.scalatra.json.JacksonJsonSupport
import scala.util.Try


class RedSkyRetail(db: Option[MongoDatabase]) extends ScalatraServlet with JacksonJsonSupport with RedSkyHTTPClient {
  /* provide an secondary constructor for the case where there's no db connection - primarily used for running under a test harness.
     The servlet itself won't care, the db instance will get passed down to lower-level code that will care but knows to expect
     that None is a possible value.
   */
  def this() = this(None)

  // use scalatra json formatters, since we only need to deal with JSON.
  protected implicit lazy val jsonFormats: Formats = DefaultFormats.withBigDecimal

  val badProductId = "{error: 'product ID not parseable or other error encountered'}"

  before() {
    contentType = formats("json")
  }

  get("/") {
    db match {
      case Some(_) => ("DB" -> true)
      case None => ("DB" -> false)
    }
  }

  get("/test") {
    ProductInfo.parse(13860428L, "The Big Lebowski (Blu-ray) (Widescreen)", BigDecimal(13.49))
  }

  /*
  If the product id isn't an integer or something else went wrong, return a 400.
  If it's parseable as a long but the back-end rest service returns a 404, also return a 404.
  If we find it, return a 200
   */
  get("/products/:id") {
    val idToFetch = params("id")

    val pid = Try(idToFetch.toLong).toOption

    pid match {
      case None => BadRequest(badProductId)
      case Some(pid) => {
        val (response, result) = fetchProductInfo(db, pid)
        // we got a :id value that can be turned into a long - determine the right result code.
        response match {
          case 200 => Ok(result)
          case 404 => NotFound(result) // the underlying rest service didn't return anything useful.
          case _ => BadRequest(badProductId)
        }
      }
    }
  }

  /* Given a well-formed docbody, update the price */
  put("/products/:id") {
    val idToFetch = params("id")

    val pid = Try(idToFetch.toLong).toOption

    pid match {
      case None => BadRequest()
      case Some(pid) => {
        val price = parsePriceInfo(request.body)
        val upd = ProductPricePersistence.savePriceInfoforId(db, pid, price)
        upd match {
          case None => BadRequest() // the insert/update failed
          case Some(x) => NoContent()
        }
      }
    }
  }
}
