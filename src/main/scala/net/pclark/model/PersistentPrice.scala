package net.pclark.model
import org.mongodb.scala.bson.ObjectId

object PersistentPrice {
  def apply(pid: Long, price: BigDecimal): PersistentPrice =
    PersistentPrice(new ObjectId(), pid, price, "USD")

  def apply(pid: Long, price: BigDecimal, currency:String): PersistentPrice =
    PersistentPrice(new ObjectId(), pid, price, currency)

}

case class PersistentPrice (_id : ObjectId, pid: Long, price: BigDecimal, currency:String)

