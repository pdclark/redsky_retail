package net.pclark.model

// data-bearing class for prices. It's always bad form to put a currency amount into a float.
// The currency could be represented via java.util.Currency rather than a string if warranted.
// This is essentially the domain-level version of PersistentPrice
case class PriceInfo(value:BigDecimal, currency_code: String = "USD") {

  def isUndefined = {
    this == PriceInfo.undefinedPrice
  }
}

object PriceInfo {
  val undefinedPrice = new PriceInfo(0.0, currency_code="UNK")

  def parse(v:Option[BigDecimal], c:Option[String]) : PriceInfo = {
    val currency = c match {
      case None => "USD"
      case Some(cur) => cur
    }

    v match {
      case None => undefinedPrice
      case Some(price) => new PriceInfo(price, currency)
    }
  }
}