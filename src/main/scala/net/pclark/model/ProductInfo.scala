package net.pclark.model


// data-bearing class for results
case class ProductInfo(id:Long, name:String, current_price:PriceInfo = PriceInfo.undefinedPrice) {

  def setPrice(price:BigDecimal, currency:String) = {
    new ProductInfo(id, name, new PriceInfo(price, currency) )
  }

  def setPrice(price:Option[BigDecimal]) = {
    price match {
      case None => this
      case Some(x) => new ProductInfo(id, name, new PriceInfo(x))
    }
  }
}

object ProductInfo {
  // companion object that provides (probably too many) factory methods to produce ProductInfo instances

  def notFound(id:Long):ProductInfo = {
    ProductInfo(id, "Not Found")
  }

  def parse(id:String, name:String):ProductInfo = {
    val parsedId = id.toLong
    new ProductInfo(parsedId, name)
  }

  // this is the most relevant parse() method.
  // The JSON parser provides Option values, which gives some robustness in the face of bad input:
  // the JSON parser will return an NONE rather than null or throwing. The possibility of receiving a NONE
  // is handled here.
  def parse(id:Option[String], name:Option[String], price:Option[BigDecimal]):ProductInfo = {
    val parsedId = id match {
      case Some(s) => s.toLong
      case None => -1L
    }

    val parsedName = name match {
      case Some(s) => s
      case None => "Not Found"
    }

    val result = price match {
      case Some(s) => parse(parsedId, parsedName, s)
      case None => new ProductInfo(parsedId, parsedName)
    }

    result
  }

  def parse(id:String, name:String, price:String):ProductInfo = {
    val parsedPrice = BigDecimal(price)
    parse(id, name, parsedPrice)
  }


  def parse(id:String, name:String, price:BigDecimal) = {
    val parsedId = id.toLong
    val priceInfo = new PriceInfo(price, "USD")
    new ProductInfo(parsedId, name, priceInfo)
  }

  def parse(id:Long, name:String, price:BigDecimal) = {
    val priceInfo = new PriceInfo(price, "USD")
    new ProductInfo(id, name, priceInfo)
  }


}