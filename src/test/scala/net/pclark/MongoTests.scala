package net.pclark

import java.util.concurrent.TimeUnit

import org.scalatest._
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._

import scala.concurrent.Await
import scala.concurrent.duration._

/*
These tests are more about exploring the MongoDB Scala API
 */
class MongoTests extends FlatSpec {

  // fixture to manage mongo connection. In the interest of being stateless and concurrent,
  // each test execution gets its own mongo connection
  def withMongo(testCode: MongoDatabase => Any) {
    println("Creating Mongo connection")
    val mongoClient: MongoClient = MongoClient("mongodb://localhost")
    val database: MongoDatabase = mongoClient.getDatabase("redsky")
    try {
      testCode(database)
    }
    finally mongoClient.close()
  }

  "Mongo" should "be accessible" in withMongo { db =>
    val collection: MongoCollection[Document] = db.getCollection("test")

    println("Got connection, querying")
    val elementCountObservable = collection.countDocuments()
    val count = Await.result(elementCountObservable.head(), 5 seconds)

    assert(count >= 1)
  }

  "Mongo" should "be insertable" in withMongo { db =>
    val collection: MongoCollection[Document] = db.getCollection("test")

    val doc = Document("""{num:5, info:"I am some test data", items:[1,2,3] }""")
    val insertResponse = collection.insertOne(doc)

    val result = Await.result(insertResponse.head(),  5 seconds)

    assert(result.isInstanceOf[Completed])
  }

  "Mongo" should "be queryable for a test document" in withMongo { db =>
    val collection: MongoCollection[Document] = db.getCollection("test")

    val queryResponse = collection.find(equal("num", 1L))

    val result = Await.result(queryResponse.head(), 5 seconds)

    assert(result.isInstanceOf[Document])
    // String fields in the Document will be BsonStrings, not Strings.
    val docField = result("info").asString.getValue

    // println("Info field from document is:" + docField)

    assert("I am test data".equals(docField))
  }
}
