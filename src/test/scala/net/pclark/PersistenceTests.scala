package net.pclark

import java.util.concurrent.TimeUnit

import net.pclark.model.PriceInfo
import org.mongodb.scala.{Completed, Document, MongoClient, MongoCollection, MongoDatabase}
import org.scalatest.FlatSpec

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class PersistenceTests extends FlatSpec {

  // fixture to manage mongo connection. In the interest of being stateless and concurrent,
  // each test execution gets its own mongo connection
  def withMongo(testCode: MongoDatabase => Any) {
    println("Creating Mongo connection")
    val mongoClient: MongoClient = MongoClient("mongodb://localhost")
    val database: MongoDatabase = mongoClient.getDatabase("redsky")
    try {
      testCode(database)
    }
    finally mongoClient.close()
  }

  // for this test to pass, there must be data seeded in mongo - see the setup.bash script
  "Price" should "be queryable" in withMongo { db =>
    val price = ProductPricePersistence.fetchPriceForID(Some(db), -1L )
    price match {
      case None => assert(false)
      case Some(x) => assert(x == BigDecimal(12.75))
    }
  }

  "Price" should "be queryable without db" in withMongo { db =>
    val price = ProductPricePersistence.fetchPriceForID(None, -1L )
    price match {
      case None => assert(true)
      case Some(x) => assert(false) // can't happen
    }
  }

  "Price" should "be insertable" in withMongo { db =>
    val id = 10L
    val price = BigDecimal(99.99)

    val dbresult = ProductPricePersistence.savePriceForId(Some(db), id, price)
    dbresult match {
      case None => assert(false)
      case Some(x) => {
        // since it worked, clean up after ourselves before checking the asserts
        ProductPricePersistence.clearPrices(Some(db), id)
        assert(x.getMatchedCount == 0)
        assert(x.getModifiedCount == 0)
        assert(x.getUpsertedId != null)
        assert(x.wasAcknowledged())
      }
    }
  }

  "PriceInfo" should "be insertable" in withMongo { db =>
    val id = 11L
    val pi = new PriceInfo(BigDecimal(45.67), "EUR")

    val dbresult = ProductPricePersistence.savePriceInfoforId(Some(db), id, pi)
    dbresult match {
      case None => assert(false)
      case Some(x) => {
        // since it worked, clean up after ourselves before checking the asserts
        ProductPricePersistence.clearPrices(Some(db), id)
        assert(x.getMatchedCount == 0)
        assert(x.getModifiedCount == 0)
        assert(x.getUpsertedId != null)
        assert(x.wasAcknowledged())
      }
    }

  }

  "Price" should "not be insertable without db" in {
    val id = 10L
    val price = BigDecimal(99.99)

    val dbresult = ProductPricePersistence.savePriceForId(None, id, price)
    dbresult match {
      case None => assert(true)
      case Some(_) => assert(false)  // can't happen
      }
    }

}
