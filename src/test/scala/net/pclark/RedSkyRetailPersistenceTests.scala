package net.pclark

import org.mongodb.scala.{MongoClient, MongoDatabase}
import org.scalatra.test.scalatest._

class RedSkyRetailPersistenceTests extends ScalatraFunSuite {

  val mongoClient: MongoClient = MongoClient("mongodb://localhost")
  val database: MongoDatabase = mongoClient.getDatabase("redsky")
  val db = Some(database)

  // these tests assume there's an active connection to mongo, so clean up afterwards
  override def afterAll(): Unit = {
    super.afterAll()
    mongoClient.close()
  }

  addServlet(new RedSkyRetail(db), "/*")

  test("GET /products with a bad product id should return 400") {
    get("/products/not_actually_a_product") {
      status should equal (400)
    }
  }

  test("GET /products on RedSkyRetail for a nonexistant product should return status 404") {
    get("/products/0") {
      status should equal (404)
    }
  }

  test("GET /products on RedSkyRetail for a Lebowski should return status 200") {
    get("/products/13860428") {
      status should equal (200)
    }
  }

  test("PUT /products/id should update the price") {
    put("/products/1234", """{"id":1234,"name":"The Big Lebowski (Blu-ray) (Metric)","current_price":{"value": 24.85, "currency_code":"CAD"}}""") {
      status should equal (204)
    }
  }


}
