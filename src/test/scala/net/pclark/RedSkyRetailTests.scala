package net.pclark


import org.scalatra.test.scalatest._
import org.json4s._
import org.json4s.jackson.JsonMethods._

class RedSkyRetailTests extends ScalatraFunSuite {

  // these tests can run (and pass) in the absence of a persistence layer.

  addServlet(classOf[RedSkyRetail], "/*")

  test("GET / on RedSkyRetail should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

  test("HTTPClient should be transformable to expected JSON") {
    get("/test") {
      val expectedResponse = parse("""{"id":13860428,
      "name":"The Big Lebowski (Blu-ray) (Widescreen)",
      "current_price":{"value": 13.49,"currency_code":"USD"}}""")

      status should equal (200)
      val actualResponse = parse(body)
      val Diff(changed, added, deleted) = expectedResponse diff actualResponse

      changed should equal(JNothing)
      added should equal(JNothing)
      deleted should equal(JNothing)
    }
  }


}
