package net.pclark.model

import org.scalatest.FlatSpec

class ProductInfoTests extends FlatSpec {
  "ProductInfo" should "handle absence of a price" in {
    val p = new ProductInfo(1234L, "test")
    assert(p.current_price == PriceInfo.undefinedPrice)
  }

}
